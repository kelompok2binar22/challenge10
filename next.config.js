/** @type {import('next').NextConfig} */

module.exports = {
    env: {
        apiKey: "AIzaSyDHZxjbW2vQma0uVSMs-GHcfWBoxyo4mg8",
        authDomain: "test-ba8c5.firebaseapp.com",
        projectId: "test-ba8c5",
        storageBucket: "test-ba8c5.appspot.com",
        messagingSenderId: "101495496139",
        appId: "1:101495496139:web:8d8fa4e3be5141cc8eaa4e"
    },
    // reactStrictMode: true,
    images: {
      domains: [
        "i.ibb.co",
        "images.pexels.com",
        "www.freepnglogos.com",
        "i.pinimg.com",
        "static.miraheze.org",
        "wallpapers.com",
        "wallpaperaccess.com",
        "firebasestorage.googleapis.com",
        "static.highsnobiety.com",
        "images.gog-statics.com",
        "previews.123rf.com",
      ],
    },
  
    // images: {
    //   loader: 'imgix',
    //   path: 'https://i.ibb.co/H4f3Hkv/profile.png',
    // },
    async rewrites() {
      return [
        {
          source: "/login",
          destination: "/auth/login",
        },
        {
          source: "/register",
          destination: "/auth/register",
        },
      ];
    },
  };
  