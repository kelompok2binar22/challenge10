/* eslint-disable react/react-in-jsx-scope */
import UserDetail from '../../components/profile/UserDetail';
// import {screen} from "@testing-library/react";
import {renderWithState} from '../../redux/reduxtest/renderWithState';
import '@testing-library/jest-dom';

beforeAll((done) => {
  done();
});
afterAll((done) => {
  done();
});
describe('Test <UserDetail /> component rendering', () => {
  const user =
  {
    uid: 'AyvCX4Es0WVj2FRAJzRyEZERkQC3',
    displayName: 'test5',
    email: 'test5@gmail.com',

  };
  test('Button class: \'btn btn-primary\'', async () => {
    try {
      renderWithState(<UserDetail score={0} dataUser={user} />, {
        initialState: {
          auth: {
            initState: {
              scoreRedux: 0,
              scoreRedux2: 12,
            },
          },
        },
      });
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});


