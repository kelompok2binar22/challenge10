/* eslint-disable padded-blocks */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable max-len */
import FormRegister from '../../components/auth/FormRegister';
import {fireEvent, screen} from '@testing-library/react';
import {renderWithState} from '../../redux/reduxtest/renderWithState';
import '@testing-library/jest-dom';

beforeAll((done) => {
  done();
});
afterAll((done) => {
  done();
});
describe('Test <FormRegister /> component rendering', () => {
  const setup = () => {
    const utils = renderWithState(<FormRegister />, {});
    const inputUsername = utils.getByLabelText('username');
    const inputEmail = utils.getByLabelText('email');
    const inputPassword = utils.getByLabelText('password');
    const withCreden = utils.getByText('Sign up with credentials');
    const button = utils.getByRole('button');
    return {
      inputUsername, inputEmail, inputPassword, withCreden, button,
      ...utils,
    };

  };
  test('Try input form Username:\'Admin\', it should be the same change value', async () => {
    try {
      const {inputUsername} = setup();
      fireEvent.change(inputUsername, {target: {value: 'Admin'}});
      expect(inputUsername.value).toBe('Admin');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test('Try input form email:\'admin@email.com\', it should be the same change value', async () => {
    try {
      const {inputEmail} = setup();
      fireEvent.change(inputEmail, {target: {value: 'admin@email.com'}});
      expect(inputEmail.value).toBe('admin@email.com');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test('Try input pasword:\'123456\', it should be the same change value', async () => {
    try {
      const {inputPassword} = setup();
      fireEvent.change(inputPassword, {target: {value: '123456'}});
      expect(inputPassword.value).toBe('123456');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test('It should be text \'Sign up with credentials\'', async () => {
    try {
      const {withCreden} = setup();
      expect(withCreden).toBeInTheDocument();
    } catch (e) {
      expect(e).toMatch('error');
    }
  },
  );
  test('Try click button, it should be the alert \'Form tidak boleh kosong !!\'', async () => {
    try {
      const {button} = setup();
      fireEvent.click(button);
      expect(screen.getByRole('alert')).toHaveTextContent('Form tidak boleh kosong !!');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test('Try click button, it should be the alert \'Masukkan alamat email yang valid !!\'', async () => {
    try {
      const {inputUsername, inputEmail, inputPassword, button} = setup();
      fireEvent.change(inputUsername, {target: {value: 'Admin'}});
      fireEvent.change(inputEmail, {target: {value: 'admin'}});
      fireEvent.change(inputPassword, {target: {value: '123456'}});
      fireEvent.click(button);
      expect(screen.getByRole('alert')).toHaveTextContent('Masukkan alamat email yang valid !!');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
  test('Try click button, it should be the alert \'Password minimal 6 karakter !!\'', async () => {
    try {
      const {inputUsername, inputEmail, inputPassword, button} = setup();
      fireEvent.change(inputUsername, {target: {value: 'Admin'}});
      fireEvent.change(inputEmail, {target: {value: 'admin@email.com'}});
      fireEvent.change(inputPassword, {target: {value: '123'}});
      fireEvent.click(button);
      expect(screen.getByRole('alert')).toHaveTextContent('Password minimal 6 karakter !!');
    } catch (e) {
      expect(e).toMatch('error');
    }
  });
});
