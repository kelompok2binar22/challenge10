/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/prop-types */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
import {Button} from 'reactstrap';
import styles from './Auth.module.css';
import {signInFacebook} from '../../redux/actions/authActions';
import {connect} from 'react-redux';
import Image from 'next/image';

function SignFacebook(props) {
  const {buttonFacebook} = props;

  const handleClick = async (e) => {
    e.preventDefault();
    await props.signInFacebook();
  };

  return (
    <div>
      <Button onClick={handleClick} outline color="secondary" className="btn mx-2 px-2">
        <Image
          src="https://www.freepnglogos.com/uploads/facebook-logo-icon/facebook-logo-clipart-flat-facebook-logo-png-icon-circle-22.png"
          alt=""
          width={20}
          height={20}
          className={styles.btn_img}
        />
        <strong>{buttonFacebook}</strong>
      </Button>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    buttonFacebook: state.auth.buttonFacebook,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signInFacebook: (creds) => dispatch(signInFacebook(creds)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignFacebook);
