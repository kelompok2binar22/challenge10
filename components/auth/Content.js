/* eslint-disable require-jsdoc */
/* eslint-disable react/react-in-jsx-scope */
import Link from 'next/link';
import {Button} from 'reactstrap';

export default function Content() {
  return (
    <div>
      <h1 className="display-4">K-II Gaming Platform</h1>
      <p className="lead">
       Best Game Playing Today.
      </p>
      <hr className="my-2" />
      <p className="lead">
        <Link href="/" passHref>
          <Button color="primary" outline>Kembali</Button>
        </Link>
      </p>
    </div>
  );
}
