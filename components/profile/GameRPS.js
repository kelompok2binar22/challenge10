/* eslint-disable react/no-unknown-property */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
import {collection, getDocs, orderBy, query} from 'firebase/firestore';
import {useEffect, useState} from 'react';
import {Table} from 'reactstrap';
import {db} from '../../firebase/config';

export default function GameRPS() {
  const [RPS, setRPS] = useState([]);

  async function getRPS() {
    try {
      const array = [];
      const q = query(collection(db, 'rps_game_points'), orderBy('total', 'desc'));
      const querySnapshot = await getDocs(q);
      // const querySnapshot = await getDocs(collection(db, "rps_game_points"));
      querySnapshot.forEach((doc) => {
        array.push(doc.data());
      });
      setRPS(array);
    } catch (error) {
      // console.log(error.code)
    }
  }

  useEffect(() => {
    getRPS();
  }, []);

  return (
    <div>
      <h5 className="text-center">Gaming Score</h5>
      <Table borderless hover responsive size="sm" className="text-mode">
        <thead>
          <tr>
            <th>Name</th>
            <th>Score</th>
            <th>Last Play</th>
          </tr>
        </thead>
        <tbody>
          {RPS.map((item, index) => (
            <tr index={item.id} key={index}>
              <th>{item.username}</th>
              <td>{item.total} point</td>
              <td>{item.updated_at}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      {RPS == '' ? <p className="text-danger text-center">You haven&apos;t played it yet</p> : ''}
    </div>
  );
}
