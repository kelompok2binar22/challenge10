/* eslint-disable new-cap */
/* eslint-disable require-jsdoc */
/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable max-len */
import {Button} from 'reactstrap';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import {useEffect, useState} from 'react';
import Link from 'next/link';

export default function DownloadPDF() {
  const [, setDataUser] = useState('');
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const user = JSON.parse(localStorage.getItem('user'));
      setDataUser(user);
    }
  }, []);

  const printDocument = () => {
    const input = document.getElementsByClassName('invoicePages');

    const pdf = new jsPDF();

    for (let i = 0; i < input.length; i++) {
      html2canvas(input[i]).then((canvas) => {
        const imgData = canvas.toDataURL('image/png');
        pdf.addImage(imgData, 'JPEG', 0, 0);
        if (input.length - 1 === i) {
          pdf.save('my-profile.pdf');
        } else {
          pdf.addPage();
        }
      });
    }
  };

  return (
    <div>
      <div className="mt-2 text-center">
        <Link href="/profile/views" passHref>
          <Button className="mx-3" color="info">PDF Upload & View</Button>
        </Link>
        <Button className="mx-3" color="dark" onClick={printDocument}>Print Profile</Button>
      </div>
      <div className='text-center my-3'>
      </div>
    </div>
  );
}
