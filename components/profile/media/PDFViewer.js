/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
import {useState} from 'react';
import {Document, Page, pdfjs} from 'react-pdf';
import {Button, Col, Container, Input, Label, Row} from 'reactstrap';
pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

export default function PDFViewer() {
  const [file, setFile] = useState('/pdf/sample.pdf');
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({numPages}) {
    setNumPages(numPages);
    setPageNumber(1);
  }

  function changePage(offSet) {
    setPageNumber((prevPageNumber) => prevPageNumber + offSet);
  }

  function changePageBack() {
    changePage(-1);
  }

  function changePageNext() {
    changePage(+1);
  }

  function onFileChange(event) {
    setFile(event.target.files[0]);
  }

  // function onDocumentLoadSuccess({numPages: nextNumPages}) {
  //   setNumPages(nextNumPages);
  // }

  return (
    <Container>
      <Row>
        <Col xs="auto" md="6" lg="6">
          <h4>PDF Viewer</h4>
          <div className="mx-5 px-3">
            <Label htmlFor="file">Load from file:</Label>
            <Input onChange={onFileChange} type="file" />
          </div>
          <div className="mt-4">
            {
              numPages ? <p> Page {pageNumber} of {numPages}</p> : ''
            }

            {pageNumber > 1 &&
              <Button className="mx-3" size="sm" onClick={changePageBack} color="primary">Previous Page</Button>
            }
            {
              pageNumber < numPages &&
              <Button className="mx-3" size="sm" onClick={changePageNext} color="primary">Next Page</Button>
            }
          </div>
        </Col>
        <Col xs="auto" md="6" lg="6">
          <div>
            <Document file={file} onLoadSuccess={onDocumentLoadSuccess}>
              <Page height="550" pageNumber={pageNumber} />
            </Document>
          </div>
        </Col>
      </Row>
      {/* <div>
        <Document file={file} onLoadSuccess={onDocumentLoadSuccess}>
          {Array.from({length: numPages}, (_, index) => (
            <Page
              key={`page_${index + 1}`}
              pageNumber={index + 1}
              renderAnnotationLayer={false}
              renderTextLayer={false}
            />
          ))}
        </Document>
      </div> */}
    </Container>
  );
}

