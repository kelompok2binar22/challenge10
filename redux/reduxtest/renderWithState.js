/* eslint-disable react/prop-types */
/* eslint-disable max-len */
import React from 'react';
import {render} from '@testing-library/react';
import {createStore, compose, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import reducer from '../reducers/rootReducer';
import {getFirebase} from 'react-redux-firebase';
import thunk from 'redux-thunk';


export const renderWithState = (
    ui,
    {...renderOptions} = {},
) => {
  const store = createStore(reducer, compose(applyMiddleware(thunk.withExtraArgument({getFirebase}))));
  const Wrapper = ({children}) => (
    <Provider store={store}>{children}</Provider>
  );

  return render(ui, {wrapper: Wrapper, ...renderOptions});
};
