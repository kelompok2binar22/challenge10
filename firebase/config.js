import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getFirestore} from 'firebase/firestore';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {

  apiKey: 'AIzaSyDHZxjbW2vQma0uVSMs-GHcfWBoxyo4mg8',
  authDomain: 'test-ba8c5.firebaseapp.com',
  projectId: 'test-ba8c5',
  storageBucket: 'test-ba8c5.appspot.com',
  messagingSenderId: '101495496139',
  appId: '1:101495496139:web:8d8fa4e3be5141cc8eaa4e',
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);
