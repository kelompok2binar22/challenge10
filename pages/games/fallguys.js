/* eslint-disable max-len */
import React, {useRef, useEffect, useState} from 'react';
import ReactPlayer from 'react-player';
import Layout from '../../components/layouts/Layout';
import Router from 'next/router';


const sources = {
  youtube: 'https://www.youtube.com/watch?v=Wj3dUvGLjNQ',
};

const Fallguys = () => {
  const [, setDisplayName] = useState('');
  const [, setEmail] = useState('');
  const [, setUid] = useState('');
  const ref = useRef();
  const [source] = useState(sources.youtube);
  const [playing] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const user = JSON.parse(localStorage.getItem('user'));
    if (!token) {
      Router.push('/login');
    } else {
      setEmail(user.email);
      setDisplayName(user.displayName);
      setUid(user.uid);
    }
  }, []);
  return (
    <div>
      <Layout title="Fallguys">
        <div className='container-fluid bg-white'>
          <div className='row justify-content-center'>
            <div data-aos='fade-right' className='col-6 px-5 py-5 game-text'>

              <h1 className='text-game-title'>Fallguys</h1>
              <p>Welcome to Fall Guys: Free for All! You are invited to dive and dodge your way to victory in the pantheon of clumsy. Rookie or pro? Solo or partied up? Fall Guys delivers ever-evolving, high-concentrated hilarity and fun!</p>
              <div>

              </div>
            </div>
            <div data-aos='fade-left' className='col-6 px-5 py-5 img-game'>
              <ReactPlayer className='react-player'
                ref={ref}
                url={source}
                playing={playing}
              />
            </div>
          </div>
          <div className="row px-5 py-5 bg-light">

          </div>
          <div className='row bg-light'>
            <div className='text-center mt-2 mb-5'>
              {/* <Link href='#'><Button className='btn-list btn-all-game'>SEE ALL GAME ACHIEVEMENT</Button></Link> */}
            </div>
          </div>

        </div>
      </Layout>
    </div>
  );
};

export default Fallguys;
