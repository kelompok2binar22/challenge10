/* eslint-disable max-len */
import React, {useRef, useEffect, useState} from 'react';
import ReactPlayer from 'react-player';
import Layout from '../../components/layouts/Layout';
import Router from 'next/router';


const sources = {
  youtube: 'https://www.youtube.com/watch?v=8X2kIfS6fb8',
};

const Cyberpunk = () => {
  const [, setDisplayName] = useState('');
  const [, setEmail] = useState('');
  const [, setUid] = useState('');
  const ref = useRef();
  const [source] = useState(sources.youtube);
  const [playing] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const user = JSON.parse(localStorage.getItem('user'));
    if (!token) {
      Router.push('/login');
    } else {
      setEmail(user.email);
      setDisplayName(user.displayName);
      setUid(user.uid);
    }
  }, []);
  return (
    <div>
      <Layout title="Cyberpunk">
        <div className='container-fluid bg-white'>
          <div className='row justify-content-center'>
            <div data-aos='fade-right' className='col-6 px-5 py-5 game-text'>

              <h1 className='text-game-title'>CYBERPUNK</h1>
              <p>Cyberpunk 2077 is an open-world, action-adventure RPG set in the megalopolis of Night City, where you play as a cyberpunk mercenary wrapped up in a do-or-die fight for survival. Improved and featuring all-new free additional content, customize your character and playstyle as you take on jobs, build a reputation, and unlock upgrades.</p>
              <div>

              </div>
            </div>
            <div data-aos='fade-left' className='col-6 px-5 py-5 img-game'>
              <ReactPlayer className='react-player'
                ref={ref}
                url={source}
                playing={playing}
              />
            </div>
          </div>
          <div className="row px-5 py-5 bg-light">

          </div>
          <div className='row bg-light'>
            <div className='text-center mt-2 mb-5'>
              {/* <Link href='#'><Button className='btn-list btn-all-game'>SEE ALL GAME ACHIEVEMENT</Button></Link> */}
            </div>
          </div>

        </div>
      </Layout>
    </div>
  );
};

export default Cyberpunk;
