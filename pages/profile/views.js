/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
import Layout from '../../components/layouts/Layout';
import dynamic from 'next/dynamic';

const PDFComponent = dynamic(() => import('../../components/profile/media/PDFViewer'), {
  ssr: false,
});

export default function views() {
  return (
    <>
      <Layout title="PDF Reader">
        <div className="pt-3">
          <center>
            <PDFComponent />
          </center>
        </div>
        <br />
        <br />
        <br />
      </Layout>
    </>
  );
}
